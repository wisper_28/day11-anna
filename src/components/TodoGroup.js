/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:53:19
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 19:56:17
 * @Description: file content
 * @FilePath: \todo-list\src\components\TodoGroup.js
 */
import TodoItem from "./TodoItem";

const TodoGroup = (props) => {
    return (
        <div>
            {
                props.todoList.map((value, index) => (<TodoItem key={index} value={value}></TodoItem>))
            }
        </div>
    )
}

export default TodoGroup;
