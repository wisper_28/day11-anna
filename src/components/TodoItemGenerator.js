/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:53:52
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 20:14:52
 * @Description: file content
 * @FilePath: \todo-list\src\components\TodoItemGenerator.js
 */
import { useState } from "react";

const TodoItemGenerator = (props) => {

    const [content, setContent] = useState('');

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = () => {
        if(content==='') return;
        props.onChange(content);
        setContent('');
    }

    return (
        <div>
            <input type="text" value={content} onChange={handleChangeInput}></input>
            <button onClick={handleChange}>add</button>
        </div>
    )
}

export default TodoItemGenerator;
