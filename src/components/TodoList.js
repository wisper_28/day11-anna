/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:54:08
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 20:16:14
 * @Description: file content
 * @FilePath: \todo-list\src\components\TodoList.js
 */
import { useState } from "react";
import TodoGroup from "./TodoGroup";
import TodoItemGenerator from "./TodoItemGenerator";
import './../style/todolist.css'

const TodoList = () => {
    const [todoList, setTodoList] = useState([]);

    const handleUpdateTodoList = (newTodo) => {
        setTodoList([...todoList, newTodo]);
    }

    return (
        <div>
            <p className="todo-list">Todo List</p>
            <TodoGroup todoList={todoList}></TodoGroup>
            <TodoItemGenerator onChange={handleUpdateTodoList}></TodoItemGenerator>
        </div>
    )
}

export default TodoList;
