/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:53:31
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 19:56:33
 * @Description: file content
 * @FilePath: \todo-list\src\components\TodoItem.js
 */
import '../style/todoItem.css';

const TodoItem = (props) => {
    return (
        <div className="todo-item">
            {props.value}
        </div>
    )
}

export default TodoItem;
